/*
 * This program is a very simply update agent.
 * Configure it as a user's shell. Then when that user scp's a file
 * will attempt to do a system update with said file.
 *
 * update package must be an uncompressed tarball
 * update.tar must contain only:
 *  update.tar.xz       - an xz compressed tarball.
 *  update.tar.xz.sig   - gpg signature. update.tar.xz will only be
 *                          decompressed if it matches
 *
 * The update tarball must NOT contain any other files.
 * The contents of update.tar.xz are largely undefined, however it must
 * contain an executable 'update-install' in it's root directory.
 * update-shell will (indirectly) run update-install as root if the tarball
 * signature matches a key in it's keyring.
 */
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <dirent.h>

bool validate_fsents()
{
    static const char *acceptable[] = {
        ".",
        "..",
        "update.tar",
        "update.tar.xz",
        "update.tar.xz.sig"
    };
    int lsize = sizeof(acceptable)/sizeof(acceptable[0]);
    int i;
    int j;
    DIR *curdir = opendir(".");
    struct dirent *dent;
    for (i = 0, dent = readdir(curdir);dent && (i < 5)  ;i++, dent = readdir(curdir))
    {
        j = 0;
        for (j = 0; strcmp(acceptable[j], dent->d_name) && j < lsize; j++);

        if (j > lsize)
            return false;
    }
    return i == lsize;
}

/*
 * This program would *almost* be a good program to actually be a shell script.
 * However major reason it is a 'C' program is that safe argument parsing,
 * is much harder in shell.
 */
int main(int argc, char **argv)
{
    static const char incoming_scp[] = "scp -t";
    static const char outgoing_scp[] = "scp -f";
    int exit_code = EXIT_FAILURE;
    char *system_cmd;
    char tempdir[] = "/tmp/update.XXXXXX";
    if (argc < 2)
    {
        dprintf(2, "WARNING: raw shell mode unimplemented\n");
        exit(EXIT_FAILURE);
    }
    else if (strcmp("-c", argv[1]))
    {
        dprintf(2, "First argument must be shell command to run\n");
        exit(EXIT_FAILURE);
    }
    else if (memcmp("scp ", argv[2], 4))
    {
        dprintf(2, "scp is the only supported command\n");
        dprintf(2, "cmd recieved: \"%s\"\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    if (memcmp(incoming_scp, argv[2], sizeof(outgoing_scp) - 1))
    {
        dprintf(2, "Only recieving updates supported (sending files is not)\n");
        exit(EXIT_FAILURE);
    }

    if (!mkdtemp(tempdir) ||
        chdir(tempdir))
    {
        dprintf(2, "Allocation of temporary directory has failed\n");
        goto fail_tmpdir;
    }

    /* If we were sent an update, save it to a fixed file name.
     * That will make some suff much simpler*/
    if (system("scp -t ./update.tar"))
    {
        dprintf(2, "scp failed\n");
        goto fail_tmpdir;
    }
    /* check fail codes */
    if (system("tar -xf update.tar"))
    {
        dprintf(2, "ERROR: unable to parse update tar file\n");
        goto fail_tmpdir;
    }
    /* check that exactly the right files exist */
    {
        struct stat tstat;
        /* check that the file exist and that there's no extras,
         * which would mean that the tarball only contained two files
         */
        if (!validate_fsents())
        {
            /* This should really be some sort of function */
            dprintf(2, "scp invalid tarball contents (%m)\n");
            goto fail_tmpdir;
        }
    }
    if (system("gpgv update.tar.xz.sig update.tar.xz"))
    {
        dprintf(2, "update failed signature test;\n");
        goto fail_tmpdir;
    }
    /* We invoke the suid binary update-su with an argument of the temp dir.
     * it will do a tiny amount of sanity checking (the update user is suppoed
     * to be well trusted, and only invoked for updates) and then run the
     * 'update-install' as root.
     * That is safe & sane at this point because the user had access (password
     * and/or ssh keys) to the update account, AND the update was signed by
     * a trusted key.
     *
     * So if you dont want to allow downgrades, for example, you must be sure
     * to never sign an update that skips downgrade checks.
     * (however note that you can not really prevent downgrades since u-boot
     * console access is sufficent to load any desired code, but that would
     * prevent remote downgrades even by people with access to the update
     * account)
     */
    if ((asprintf(&system_cmd, "update-su --source %s", tempdir) > 0) &&
        !system("cat update.tar.xz | unxz | tar -x") &&
        !system(system_cmd))
        exit_code = EXIT_SUCCESS;

    /* Note: it is, and should be valid for the update-install to issue a reboot,
     * reboot -f, or a kexec. Therefore we can not put any critical code here.
     * which is fine since at this point it's mostly just clean up*/
    free(system_cmd);
    system_cmd = NULL;

fail_tmpdir:
    /* it's ok if this fails */
    {
        int unusued __attribute((unused)) = chdir ("/");
    }
    /* If we could not allocate memory, then give up.
     * there's really nothing that can be done, and we were just trying
     * to remote a dir in a tmpfs. */
    if (asprintf(&system_cmd, "rm -rf %s", tempdir) > 0)
    {
        int unusued __attribute((unused)) =
            system(system_cmd);
        free(system_cmd);
    }

    return exit_code;
}
