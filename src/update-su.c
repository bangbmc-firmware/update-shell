/*
 *
 * Fairly straight forward program.
 * This program allows the update user to invoke update-install inside an
 * update as root.
 *
 * Needs to be root:update owned.
 * with suid.
 *
 * Upon two conditions:
 * 1. requires a source directory (path must follow the update-shell directory
 *      creation format).
 * 2. checks that the invoke either is in the update group,
 *      or is the update user.
 *
 * If those two conditions are met, the program update-install inside
 * of the source directory is executed as root.
 *
 */
#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define UPDATE_USER_NAME "update"
#define UPDATE_GROUP_NAME "update"

/* This enum must *not* have gaps in it, otherwise the arg
 * parsing will fail. Since a null entry marks the end of the table*/
enum {
    source_arg = 0,
};

struct option long_opts[] = {
    [source_arg] = { "source", required_argument, NULL, 0},
    { }
};

/*
 * Scan through our save user id, group id,
 * and supplementary groups. If any of the group id's
 * are the update group, or if our saved uid is the update
 * user, the we are authorized, if not, we are not authorized.
 */
bool validate_cred()
{
    ssize_t idlist_size;
    idlist_size = getgroups(0, NULL);
    struct passwd *update_uid = getpwnam(UPDATE_USER_NAME);
    struct group  *update_gid = getgrnam(UPDATE_GROUP_NAME);
    if (idlist_size >= 0 && update_gid)
    {
        gid_t egid = -1;
        gid_t rgid = -1;
        gid_t sgid = -1;
        size_t i;
        uid_t *gids;
        /* If the group id of the update user is 0 (root) bail out, whatever
         * our env is, it is not sane.*/
        if (!update_gid->gr_gid)
            return false;

        idlist_size += 1;

        gids = calloc(idlist_size, sizeof(uid_t));
        if (gids &&
           (getgroups(idlist_size, gids + 1) > -1) &&
           !getresgid(gids + 0, &egid, &sgid))
        {
            for (i = 0; i < idlist_size; i++)
            {
                if (gids[i] == update_gid->gr_gid)
                {
                    free(gids);
                    return true;
                }
            }
        }
        free(gids);
    }

    /* The first condition obviously checks that we got a valid passwd entry,
     * For the second, if the update user is uid 0 (root) then our env is not
     * sane, lets bail out.
     */
    if (update_uid && update_uid->pw_uid)
    {
        uid_t suid = -1;
        uid_t ruid = -1;
        uid_t euid = -1;
        getresuid(&ruid, &euid, &suid);
        if (suid == update_uid->pw_uid)
            return true;
    }
    return false;
}

int main(int argc, char ** argv)
{
    if (!validate_cred())
    {
        dprintf(2, "unauthorized\n");
        exit(EXIT_FAILURE);
    }
    char *source_dir = NULL;
    {
        int i;
        int index;
        while ((i = getopt_long(argc, argv, "", long_opts, &index)) >= 0)
        {
            if (i == '?')
            {
                dprintf(2, "ERROR: unkown argument\n");
                exit(EXIT_FAILURE);
            }
            switch (index) {
                case source_arg:
                    source_dir = optarg;
                    break;
                default:
                    /* should be unreachable */
                    dprintf(2, "ERROR: known argument, but case not coded\n");
                    exit(EXIT_FAILURE);
                    break;
            }
        }
    }
    if (!source_dir)
    {
        dprintf(2, "ERROR: source not given\n");
        exit(EXIT_FAILURE);
    }
    {
        static const char template[] = "/tmp/update.XXXXXX";
        static const char match[] = "/tmp/update.";
        if ((strlen(source_dir) == sizeof(template) - 1) &&
            !(memcmp(source_dir, match, sizeof(match) - 1)) &&
            !chdir(source_dir))
        {
            char *args[] = {
                "./update-install",
                "--source",
                source_dir,
                NULL
            };
            execv(args[0], args);
        }
    }

    return EXIT_FAILURE;
}
